defmodule Now.CLI do
  def main(args \\ []) do
    case args do
      [] -> Now.list()
      ["init"] -> Now.init()
    end
  end
end
