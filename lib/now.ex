defmodule Now do
  @projectfile "#{System.user_home()}/.now"

  def list() do
    nows() |> Enum.join("\n") |> IO.puts()
  end

  def init() do
    case File.read(@projectfile) do
      {:ok, file} ->
        IO.puts("Error: now-file already exists!")
        exit({:shutdown, 1})

      {:error, :enoent} ->
        File.write!(@projectfile, nowstring("Initialised now!"))
        IO.puts("Initialised now-file!")
    end
  end

  defp timestamp() do
    {stamp, result} = System.cmd("date", ["+%Y-%m-%dT%H:%M:%S%z"])
    if result == 0, do: String.trim(stamp)
  end

  defp nowstring(string) do
    "#{timestamp()} - #{string}"
  end

  defp nows() do
    file() |> String.trim() |> String.split("\n")
  end

  defp file() do
    case File.read(@projectfile) do
      {:ok, file} ->
        file

      {:error, :enoent} ->
        IO.puts("Error: now-file does not exist.\nInitiate now-file with `now init`.")
        exit({:shutdown, 1})
    end
  end
end
